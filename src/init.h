// TODO

#ifndef INIT
#define INIT

#include "mains.h"
#include "motor.h"
#include "filesystem.h"

class Init : public Filesystem
{
    public:
        Init();
        ~Init();
        bool InitSDL();
        void DeInitSDL();
        void DeInitSDLIMG();
        void DeInitPhysFS();
        int InitSDLIMG();
};

Init::Init()
{
    InitSDL();
    InitSDLIMG();
    Init_PhysFS();
}

Init::~Init()
{
    DeInitSDL();
    DeInitSDLIMG();
    DeInitPhysFS();
}

bool Init::InitSDL()
{
    if (SDL_Init(SDL_INIT_EVERYTHING == -1)) {
        return false;
    }

    std::cout << "System init\n";
    return true;

}

void Init::DeInitSDL()
{
    std::cout << "System deinit\n";
    SDL_Quit();
}

int Init::InitSDLIMG()
{
    IMG_Init(IMG_INIT_PNG);
    std::cout << "SDL_IMG is init\n";
}

void Init::DeInitSDLIMG()
{
    std::cout << "SDL_IMG is deinit\n";
    IMG_Quit();
}

void Init::DeInitPhysFS()
{
    std::cout << "PhysFS is deinit\n";
    PHYSFS_deinit();
}

#endif
