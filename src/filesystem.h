#ifndef FILESYSTEM
#define FILESYSTEM

#include "mains.h"
#include "motor.h"

class Filesystem
{
    public:
        const std::string BaseDir;
        const std::string Dir;
        const std::string FChck;
        int Priority;
    public:
        Filesystem();
        ~Filesystem();
         int Init_PhysFS();
         void SetBaseDir(const std::string BaseDir);
         int FileChck(const std::string FChck);
         int OpenFile(const std::string PhysFile);
};

Filesystem::Filesystem()
{

}

Filesystem::~Filesystem()
{

}

int Filesystem::Init_PhysFS()
{
    PHYSFS_init(NULL);
    std::cout << "PhysFS is init\n";
}

void Filesystem::SetBaseDir(const std::string BaseDir)
{
    PHYSFS_mount(BaseDir.c_str(), NULL, 1);
}

int Filesystem::FileChck(const std::string FChck)
{
    PHYSFS_exists(FChck.c_str());

    if (PHYSFS_exists(FChck.c_str()) == 0) {
        std::cout << "File exists in archive " << Dir << "\n";
        return 0;
    }
    else if (PHYSFS_exists(FChck.c_str()) != 0) {
        std::cout << "File doesn't exist\n";
    }
}

int Filesystem::OpenFile(const std::string PhysFile)
{
    PHYSFS_openRead(PhysFile.c_str());
    return 0;
}

#endif
